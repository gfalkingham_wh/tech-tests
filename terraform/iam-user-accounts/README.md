# iam-user-accounts

This is a simple product created for Tech Tests which deploys IAM user accounts and groups with permission to access S3 buckets.  

In order to mock up the IAM user/group access, S3 buckets are created by this product.  

Internal users should be added to either one or the other of these lists in the dev and/or prod params: 

* internal_users_basic
* internal_users_privileged

IAM users are allocated to the apropriate IAM group based upon which list they are in.  

In this manner, internal IAM users can be given either basic or privileged access to the S3 buckets in either environment.  

# Usage
```bash
Example goes here
```
## Configuration
No configuration necessary. 

## Environment Variables
No environment variables.

# Testing
No tests developed yet. 

# Terraform Docs
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.0.2 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.49.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.50.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_tags"></a> [tags](#module\_tags) | git::https://gitlab.com/williamhillplc/technical-services/public-cloud/terraform-modules/tf-aws-williamhill-tags.git | 5.1.0 |

## Resources

| Name | Type |
|------|------|
| [aws_iam_group.internal_users_basic_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group) | resource |
| [aws_iam_group.internal_users_privileged_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group) | resource |
| [aws_iam_group_membership.internal_users_basic_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_membership) | resource |
| [aws_iam_group_membership.internal_users_privileged_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_membership) | resource |
| [aws_iam_group_policy.s3_policy_permissive](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy) | resource |
| [aws_iam_group_policy.s3_policy_restrictive](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy) | resource |
| [aws_iam_user.internal_users](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_s3_bucket.techtest](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_public_access_block.techtest](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_tags"></a> [additional\_tags](#input\_additional\_tags) | Map of tags that will be applied to everything where possible. | `map(any)` | `{}` | no |
| <a name="input_approval"></a> [approval](#input\_approval) | Release management approval reference | `string` | n/a | yes |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | Region to be deployed into. | `string` | n/a | yes |
| <a name="input_aws_role"></a> [aws\_role](#input\_aws\_role) | Role used in Makefile for TF backend configuration | `string` | n/a | yes |
| <a name="input_bucket_names"></a> [bucket\_names](#input\_bucket\_names) | List of bucket names. | `list(string)` | `[]` | no |
| <a name="input_channel"></a> [channel](#input\_channel) | Name of the channel | `string` | n/a | yes |
| <a name="input_channel_account_id"></a> [channel\_account\_id](#input\_channel\_account\_id) | AWS account ID for the target channel. | `string` | n/a | yes |
| <a name="input_channel_short"></a> [channel\_short](#input\_channel\_short) | Short channel name. | `string` | `""` | no |
| <a name="input_create_bucket"></a> [create\_bucket](#input\_create\_bucket) | Enables the creation of the S3 bucket. | `bool` | `false` | no |
| <a name="input_description"></a> [description](#input\_description) | Description of the lambda. | `string` | `"Reads a summary of newCasesByPublishDate from the UK Government’s COVID-19 data in CSV format."` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name | `string` | n/a | yes |
| <a name="input_git_ref"></a> [git\_ref](#input\_git\_ref) | Version tag or feature branch name | `string` | n/a | yes |
| <a name="input_gitrepo"></a> [gitrepo](#input\_gitrepo) | Git repo location | `string` | n/a | yes |
| <a name="input_internal_users_basic"></a> [internal\_users\_basic](#input\_internal\_users\_basic) | List of internal users with basic access to S3 buckets. | `list(string)` | `[]` | no |
| <a name="input_internal_users_privileged"></a> [internal\_users\_privileged](#input\_internal\_users\_privileged) | List of internal users with privileged access to S3 buckets. | `list(string)` | `[]` | no |
| <a name="input_name"></a> [name](#input\_name) | Used to name lots of objects. be wary of changing after deployment. | `string` | `"ghf-covid-stats-lambda"` | no |
| <a name="input_org"></a> [org](#input\_org) | Organisation prefix | `string` | n/a | yes |
| <a name="input_product"></a> [product](#input\_product) | Product name | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
