resource "aws_iam_group_membership" "internal_users_basic_access" {
  name  = "Internal-Users"
  users = var.internal_users_basic
  group = aws_iam_group.internal_users_basic_group.name
}

resource "aws_iam_group" "internal_users_basic_group" {
  name = "Internal-Users-Basic"
  path = "/user_groups/"
}

resource "aws_iam_group_policy" "s3_policy_restrictive" {
  name  = "S3-Policy-Restrictive"
  group = aws_iam_group.internal_users_basic_group.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "S3PolicyRestrictive1"
        Action = [
          "s3:GetBucketLocation",
          "s3:ListBucket",
        ]
        Effect   = "Allow"
        Resource = local.policy_resources
      },
      {
        Sid = "S3PolicyRestrictive2"
        Action = [
          "s3:ListAllMyBuckets",
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}
