resource "aws_iam_user" "internal_users" {
  for_each = local.internal_users
  name     = each.key
  tags     = module.tags.map
}
