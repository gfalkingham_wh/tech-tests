resource "aws_iam_group_membership" "internal_users_privileged_access" {
  name  = "Internal-Users"
  users = var.internal_users_privileged
  group = aws_iam_group.internal_users_privileged_group.name
}

resource "aws_iam_group" "internal_users_privileged_group" {
  name = "Internal-Users-Privileged"
  path = "/user_groups/"
}

resource "aws_iam_group_policy" "s3_policy_permissive" {
  name  = "S3-Policy-Permissive"
  group = aws_iam_group.internal_users_privileged_group.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = "S3PolicyPermissive1"
        Action = [
          "s3:GetBucketLocation",
          "s3:GetObject",
          "s3:PutObject",
          "s3:PutObjectRetention",
          "s3:PutObjectTagging",
          "s3:ListBucket",
          "s3:DeleteObject"
        ]
        Effect   = "Allow"
        Resource = local.policy_resources
      },
      {
        Sid = "S3PolicyPermissive2"
        Action = [
          "s3:ListAllMyBuckets",
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}
