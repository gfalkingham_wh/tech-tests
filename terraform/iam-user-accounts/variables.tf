variable "org" {
  description = "Organisation prefix"
  type        = string
}

variable "channel" {
  description = "Name of the channel"
  type        = string
}

variable "aws_role" {
  description = "Role used in Makefile for TF backend configuration"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
}

variable "aws_region" {
  description = "Region to be deployed into."
  type        = string
}

variable "bucket_names" {
  description = "List of bucket names."
  type        = list(string)
  default     = []
}

variable "internal_users_basic" {
  description = "List of internal users with basic access to S3 buckets."
  type        = list(string)
  default     = []
}

variable "internal_users_privileged" {
  description = "List of internal users with privileged access to S3 buckets."
  type        = list(string)
  default     = []
}

variable "channel_short" {
  description = "Short channel name."
  type        = string
  default     = ""
}

variable "additional_tags" {
  description = "Map of tags that will be applied to everything where possible."
  type        = map(any)
  default     = {}
}

variable "name" {
  description = "Used to name lots of objects. be wary of changing after deployment."
  type        = string
  default     = "ghf-covid-stats-lambda"
}

variable "description" {
  description = "Description of the lambda."
  type        = string
  default     = "Reads a summary of newCasesByPublishDate from the UK Government’s COVID-19 data in CSV format."
}

variable "create_bucket" {
  description = "Enables the creation of the S3 bucket."
  type        = bool
  default     = false
}

variable "channel_account_id" {
  description = "AWS account ID for the target channel."
  type        = string
}

variable "product" {
  description = "Product name"
  type        = string
}

variable "approval" {
  description = "Release management approval reference"
  type        = string
}

variable "git_ref" {
  description = "Version tag or feature branch name"
  type        = string
}

variable "gitrepo" {
  description = "Git repo location"
  type        = string
}
