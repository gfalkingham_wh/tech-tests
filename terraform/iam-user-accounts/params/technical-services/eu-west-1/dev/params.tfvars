bucket_names = [
  "Fancy-Bucket-A",
  "Fancy-Bucket-B",
]

internal_users_basic = [
  "Internal-User-One"
]

internal_users_privileged = [
  "Internal-User-Two",
  "Internal-User-Three"
]
