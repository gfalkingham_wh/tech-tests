resource "aws_s3_bucket" "techtest" {
  #tfsec:ignore:AWS002
  #tfsec:ignore:AWS017
  #tfsec:ignore:AWS077
  #tfsec:ignore:AWS098
  depends_on    = [aws_iam_group_policy.s3_policy_permissive, ]
  for_each      = local.bucket_names
  bucket        = "${var.org}-${var.channel_short}-${var.environment}-${var.aws_region}-${var.product}-${each.key}"
  acl           = "private"
  force_destroy = false
  tags          = module.tags.map
}

resource "aws_s3_bucket_public_access_block" "techtest" {
  for_each = local.bucket_names
  bucket   = aws_s3_bucket.techtest[each.key].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
