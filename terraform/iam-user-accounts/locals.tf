locals {
  bucket_names              = toset(var.bucket_names)
  internal_users_basic      = toset(var.internal_users_basic)
  internal_users_privileged = toset(var.internal_users_privileged)
  internal_users            = toset(concat(var.internal_users_basic, var.internal_users_privileged))
  policy_resources          = jsonencode([for item in var.bucket_names : "arn:aws:s3:::${var.org}-${var.channel_short}-${var.environment}-${var.aws_region}-${var.product}-${item}"])
}
