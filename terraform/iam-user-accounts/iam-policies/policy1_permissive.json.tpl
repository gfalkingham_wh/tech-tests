{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "statement1",
      "Effect" = "Allow",
      "Action": [
        "s3:GetBucketLocation",
        "s3:GetObject",
        "s3:PutObject",
        "s3:PutObjectRetention",
        "s3:PutObjectTagging",
        "s3:ListBucket",
        "s3:ListAllMyBuckets",
        "s3:DeleteObject"
      ],
      "Resource" = [
        "${bucket_arn}"
      ]
    }
  ]
}