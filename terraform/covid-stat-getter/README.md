# get-covid-stats

This is a simple python lambda to read a summary of `newCasesByPublishDate` from the UK Government’s COVID-19 data in CSV format.

Data is obtained from the API here: - https://coronavirus.data.gov.uk/details/developers-guide

The CSV file is populated with the following two fields: 

* date
* newCasesByPublishDate

Nb. The number of 'new cases by publish date' is for the whole United Kingdom, and is not broken down by nation. 

# Usage
```bash
Example goes here
```
## Configuration
No configuration necessary. 

## Environment Variables
The lambda currently accepts `BUCKET_NAME` for the name of the bucket that the lambda will output the CSV file to. 
The lambda will also accept `LOG_LEVEL` which defaults to `INFO`. This is still WIP. 

# Testing
No tests developed yet. 

# Terraform Docs
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.0.2 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.49.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.50.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_lambda"></a> [lambda](#module\_lambda) | git::https://gitlab.com/williamhillplc/technical-services/public-cloud/terraform-modules/tf-aws-lambda.git | 7.0.0 |
| <a name="module_tags"></a> [tags](#module\_tags) | git::https://gitlab.com/williamhillplc/technical-services/public-cloud/terraform-modules/tf-aws-williamhill-tags.git | 5.1.0 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_event_rule.schedule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_target.schedule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_lambda_permission.schedule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.covid_stats_output](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_public_access_block.covid_stats_output](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_tags"></a> [additional\_tags](#input\_additional\_tags) | Map of tags that will be applied to everything where possible. | `map(any)` | `{}` | no |
| <a name="input_approval"></a> [approval](#input\_approval) | Release management approval reference | `string` | n/a | yes |
| <a name="input_aws_role"></a> [aws\_role](#input\_aws\_role) | Role used in Makefile for TF backend configuration | `string` | n/a | yes |
| <a name="input_channel"></a> [channel](#input\_channel) | Name of the channel | `string` | n/a | yes |
| <a name="input_channel_account_id"></a> [channel\_account\_id](#input\_channel\_account\_id) | AWS account ID for the target channel. | `string` | n/a | yes |
| <a name="input_create_bucket"></a> [create\_bucket](#input\_create\_bucket) | Enables the creation of the S3 bucket. | `bool` | `false` | no |
| <a name="input_description"></a> [description](#input\_description) | Description of the lambda. | `string` | `"Reads a summary of newCasesByPublishDate from the UK Government’s COVID-19 data in CSV format."` | no |
| <a name="input_enable_scheduler"></a> [enable\_scheduler](#input\_enable\_scheduler) | Enables the lambda to run and the specified cron\_exec value. | `bool` | `true` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Enables or disables this module. | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name | `string` | n/a | yes |
| <a name="input_git_ref"></a> [git\_ref](#input\_git\_ref) | Version tag or feature branch name | `string` | n/a | yes |
| <a name="input_gitrepo"></a> [gitrepo](#input\_gitrepo) | Git repo location | `string` | n/a | yes |
| <a name="input_log_level"></a> [log\_level](#input\_log\_level) | Sets the log level of the application as passed directly as an environment variable. | `string` | `"INFO"` | no |
| <a name="input_name"></a> [name](#input\_name) | Used to name lots of objects. be wary of changing after deployment. | `string` | `"ghf-covid-stats-lambda"` | no |
| <a name="input_org"></a> [org](#input\_org) | Organisation prefix | `string` | n/a | yes |
| <a name="input_product"></a> [product](#input\_product) | Product name | `string` | n/a | yes |
| <a name="input_schedule"></a> [schedule](#input\_schedule) | When should the lambda be invoked. | `string` | `"rate(1 day)"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
