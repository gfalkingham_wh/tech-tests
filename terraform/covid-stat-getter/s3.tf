resource "aws_s3_bucket" "covid_stats_output" {
  bucket = "${var.name}-${data.aws_region.current.name}-s3bucket"
  count  = var.create_bucket && var.enabled ? 1 : 0

  tags = var.additional_tags

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "log"
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 90
      storage_class = "GLACIER"
    }
  }
  #tfsec:ignore:AWS002
  #tfsec:ignore:AWS017
}

resource "aws_s3_bucket_public_access_block" "covid_stats_output" {
  count  = var.create_bucket && var.enabled ? 1 : 0
  bucket = aws_s3_bucket.covid_stats_output[0].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
