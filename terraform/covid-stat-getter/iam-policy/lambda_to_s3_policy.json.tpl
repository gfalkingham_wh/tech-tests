{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "statement1",
      "Effect" = "Allow",
      "Action" = [
        "s3:PutObject"
      ],
      "Resource" = [
        "${bucket_arn}"
      ]
    }
  ]
}
