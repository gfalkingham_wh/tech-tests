variable "org" {
  description = "Organisation prefix"
  type        = string
}

variable "channel" {
  description = "Name of the channel"
  type        = string
}

variable "aws_role" {
  description = "Role used in Makefile for TF backend configuration"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
}

variable "additional_tags" {
  description = "Map of tags that will be applied to everything where possible."
  type        = map(any)
  default     = {}
}

variable "enabled" {
  description = "Enables or disables this module."
  type        = bool
  default     = false
}

variable "log_level" {
  description = "Sets the log level of the application as passed directly as an environment variable."
  type        = string
  default     = "INFO"
}

variable "schedule" {
  description = "When should the lambda be invoked."
  type        = string
  default     = "rate(1 day)"
}

variable "enable_scheduler" {
  description = "Enables the lambda to run and the specified cron_exec value."
  type        = bool
  default     = true
}

variable "name" {
  description = "Used to name lots of objects. be wary of changing after deployment."
  type        = string
  default     = "ghf-covid-stats-lambda"
}

variable "description" {
  description = "Description of the lambda."
  type        = string
  default     = "Reads a summary of newCasesByPublishDate from the UK Government’s COVID-19 data in CSV format."
}

variable "create_bucket" {
  description = "Enables the creation of the S3 bucket."
  type        = bool
  default     = false
}

variable "channel_account_id" {
  description = "AWS account ID for the target channel."
  type        = string
}

variable "product" {
  description = "Product name"
  type        = string
}

variable "approval" {
  description = "Release management approval reference"
  type        = string
}

variable "git_ref" {
  description = "Version tag or feature branch name"
  type        = string
}

variable "gitrepo" {
  description = "Git repo location"
  type        = string
}
