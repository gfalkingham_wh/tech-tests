resource "aws_cloudwatch_event_rule" "schedule" {
  count               = var.enabled && var.enable_scheduler ? 1 : 0
  name                = "${local.function_name}-schedule"
  schedule_expression = var.schedule
}

resource "aws_cloudwatch_event_target" "schedule" {
  count     = var.enabled && var.enable_scheduler ? 1 : 0
  target_id = "${local.function_name}-schedule"
  rule      = element(concat(aws_cloudwatch_event_rule.schedule.*.name, [""]), 0)
  arn       = module.lambda.function_arn
}

resource "aws_lambda_permission" "schedule" {
  count         = var.enabled && var.enable_scheduler ? 1 : 0
  statement_id  = "${local.function_name}-schedule"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = element(concat(aws_cloudwatch_event_rule.schedule.*.arn, [""]), 0)
}
