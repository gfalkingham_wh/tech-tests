from covid_stat_getter.get import StatGetter


def lambda_handler(event, context):
    """Lambda Entrypoint."""
    statgetter = StatGetter()
    statgetter.run()
