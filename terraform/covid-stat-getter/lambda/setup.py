"""
Setup.

Package Setup.
"""
from setuptools import find_packages
from setuptools import setup

setup(
    name="covid_stat_getter",
    description="covid_stat_getter Python Lambda",
    url="",
    version="1.0.0",
    author="Contributors",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
    ],
    packages=find_packages(exclude=("tests*", "testing*")),
    install_requires=[
        "boto3",
    ],
)
