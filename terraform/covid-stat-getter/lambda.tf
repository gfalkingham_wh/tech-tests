module "lambda" {
  source = "git::https://gitlab.com/williamhillplc/technical-services/public-cloud/terraform-modules/tf-aws-lambda.git?ref=7.0.0"

  enabled       = var.enabled
  function_name = local.function_name
  description   = var.description
  handler       = "covid_stat_getter.main.lambda_handler"
  runtime       = "python3.8"
  timeout       = 300

  source_path = "${path.module}/lambda"

  attach_policy = true
  policy        = templatefile("${path.module}/iam-policy/lambda_to_s3_policy.json.tpl", { bucket_arn = aws_s3_bucket.covid_stats_output[0].arn })

  environment = {
    variables = {
      LOG_LEVEL   = var.log_level
      BUCKET_NAME = local.bucket_name
    }
  }

  permissions_boundary = ""
  tags                 = var.additional_tags
}

locals {
  function_name = "${var.name}-${data.aws_region.current.name}"
  bucket_name   = "${var.name}-${data.aws_region.current.name}-s3bucket"
}
