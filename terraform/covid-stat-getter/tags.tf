locals {
  required_tags = {
    SubDivision    = "TechOps"
    Product        = var.product
    Classification = "Confidential"
    CardData       = "No"
    Migrated       = "No"
    Approval       = var.approval
    Tier           = "Internal"
  }

  additional_tags = {
    Owner               = "Cloud Services"
    Escalation          = "gfalkingham@williamhill.co.uk"
    CostCentre          = "000000"
    AppRole             = "Tech Tests"
    InstanceReplacement = "true"
    Environment         = var.environment
    GitURL              = var.gitrepo
    Version             = var.git_ref
  }
}

module "tags" {
  source          = "git::https://gitlab.com/williamhillplc/technical-services/public-cloud/terraform-modules/tf-aws-williamhill-tags.git?ref=5.1.0"
  required_tags   = local.required_tags
  additional_tags = local.additional_tags
}

