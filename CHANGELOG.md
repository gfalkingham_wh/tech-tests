# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2).

Dates in this file are in format of YYYY-MM-DD (2019-12-13 means 13th of December 2019).

## [[2.0.1]](https://gitlab.com/gfalkingham_wh/get-covid-stats/-/tags/2.0.1) - 2021-07-16

### Changed

* covid-stat-getter product upgrade to terraform v1.0.2 [@gfalkingham_wh](https://gitlab.com/gfalkingham_wh) [#GHF-003](https://jira.willhillatlas.com/browse/GHF-003)

## [[2.0.0]](https://gitlab.com/gfalkingham_wh/get-covid-stats/-/tags/2.0.0) - 2021-07-16

### Added

* iam-user-accounts product stack created [@gfalkingham_wh](https://gitlab.com/gfalkingham_wh) [#GHF-002](https://jira.willhillatlas.com/browse/GHF-002)

## [[1.0.0]](https://gitlab.com/gfalkingham_wh/get-covid-stats/-/tags/1.0.0) - 2021-07-13

### Added

* Initial Commit [@gfalkingham_wh](https://gitlab.com/gfalkingham_wh) [#GHF-001](https://jira.willhillatlas.com/browse/GHF-001)
* covid-stat-getter product stack created [@gfalkingham_wh](https://gitlab.com/gfalkingham_wh) [#GHF-001](https://jira.willhillatlas.com/browse/GHF-001)
