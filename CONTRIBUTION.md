# Contributing

When contributing to this repository please ensure that you've read our git standards, an always up to date version can be found
at: https://conf.willhillatlas.com/pages/viewpage.action?pageId=255445103, and more specifically: https://conf.willhillatlas.com/display/CSDOC/Git+Standards.


## Merge Requests & Pull Requests
In order to contribute to this code you must create a MR or a PR depending on whether youre internal or external to the team.

## Merge Request Process

## README.md
terraform-docs should be used to generate your README.md, it does nice things like include inputs/outputs in a nice table.

## Latest Standards
https://gitlab.com/williamhillplc/technical-services/public-cloud/landing-zone/standards/-/blob/develop/README.md